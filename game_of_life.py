#!/usr/bin/python3
# coding: utf8
#################
# Jeu de la vie #
#################

import random
import argparse
import tkinter

class Jeu:
    """
    Classe du jeu
    """
    def __init__(self, root, taille):
        # Taille du plateau
        self.taille = taille
        self.zoom = 10
        self.pause = 1
        self.iteration = 0
        self.plateau = list()

        self.frame = tkinter.Frame(root)
        self.frame.grid()
        self.frame.after(1, self.milliSecondUpdate, root)

        self.startb = tkinter.Button(self.frame, text="Start", fg="black", command=self.start)
        self.startb.grid(row=0,column=0)

        options = [
            "Vide",
            "Random",
            "Floraison",
            "Glisseur",
            "Spaceship",
            "Pulsar",
            "Penta-decathlon",
            "Glider Gun",
        ]
        self.variable = tkinter.StringVar(self.frame)
        self.variable.set(options[0])
        self.optionb = tkinter.OptionMenu(self.frame, self.variable, *options, command=self.optionCommand)
        self.optionb.grid(row=0,column=2)

        self.canvas = tkinter.Canvas(self.frame, width=self.taille*self.zoom, height=self.taille*self.zoom)
        self.canvas.grid(row=1,column=0,columnspan=3)

        self.canvas.bind('<Button-1>', self.click)

        self.reset()

    def optionCommand(self, event=None):
        """
        Remplissage vide du plateau
        """
        self.reset()
        lg = len(self.plateau)
        choix = self.variable.get()
        if choix == "Vide":
            return
        elif choix == "Random":
            self.random()
            return
        elif choix == "Floraison":
            # Forme de U
            tableau = [
                [1,0,1],
                [1,0,1],
                [1,1,1]]
        elif choix == "Glisseur":
            tableau = [
                [1,0,0],
                [1,0,1],
                [1,1,0]]
        elif choix == "Pulsar":
            tableau = [
                [0,0,1,1,1,0,0,0,1,1,1,0,0],
                [0,0,0,0,0,0,0,0,0,0,0,0,0],
                [1,0,0,0,0,1,0,1,0,0,0,0,1],
                [1,0,0,0,0,1,0,1,0,0,0,0,1],
                [1,0,0,0,0,1,0,1,0,0,0,0,1],
                [0,0,1,1,1,0,0,0,1,1,1,0,0],
                [0,0,0,0,0,0,0,0,0,0,0,0,0],
                [0,0,1,1,1,0,0,0,1,1,1,0,0],
                [1,0,0,0,0,1,0,1,0,0,0,0,1],
                [1,0,0,0,0,1,0,1,0,0,0,0,1],
                [1,0,0,0,0,1,0,1,0,0,0,0,1],
                [0,0,0,0,0,0,0,0,0,0,0,0,0],
                [0,0,1,1,1,0,0,0,1,1,1,0,0],
            ]
        elif choix == "Penta-decathlon":
            tableau = [
                [1,1,1],
                [1,0,1],
                [1,1,1],
                [1,1,1],
                [1,1,1],
                [1,1,1],
                [1,0,1],
                [1,1,1],
            ]
        elif choix == "Spaceship":
            tableau = [
                [0,1,0,0,1],
                [1,0,0,0,0],
                [1,0,0,0,1],
                [1,1,1,1,0],
            ]
        elif choix == "Glider Gun":
            tableau = [
                [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0],
                [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0],
                [0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,1,1],
                [0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,1,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,1,1],
                [1,1,0,0,0,0,0,0,0,0,1,0,0,0,0,0,1,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                [1,1,0,0,0,0,0,0,0,0,1,0,0,0,1,0,1,1,0,0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0],
                [0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0],
                [0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                [0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            ]

        # Dessin
        i0 = lg // 2 - len(tableau[0])//2
        j0 = lg // 2 - len(tableau)//2
        for i in range(0,len(tableau[0])):
            for j in range(0,len(tableau)):
                if tableau[j][i] == 1:
                    couleur = "black"
                else:
                    couleur = "white"
                self.canvas.itemconfig(self.plateau[i+i0][j+j0],fill=couleur)
                    
    def reset(self):
        """
        Remplissage vide du plateau
        """
        self.canvas.delete(tkinter.ALL)
        self.plateau = list()
        for i in range(0,self.taille):
            ligne = list()
            for j in range(0,self.taille):
                color = "white"
                rect = self.canvas.create_rectangle(i*self.zoom, j*self.zoom, (i+1)*self.zoom, (j+1)*self.zoom, fill=color, width=0)
                ligne.append(rect)
            self.plateau.append(ligne)

    def random(self):
        """
        Remplissage aléatoire du plateau
        """
        for i in range(0,self.taille):
            for j in range(0,self.taille):
                if random.randint(0,1) == 0:
                    color = "white"
                else:
                    color = "black"
                self.canvas.itemconfig(self.plateau[i][j],fill=color)

    def voisinage(self):
        """
        Calcul du nombre de voisins
        """
        voisins = [-1,0,1]
        v = list()
        for i in range(0,self.taille):
            ligne = list()
            for j in range(0,self.taille):
                res = 0
                for v1 in voisins:
                    for v2 in voisins:
                        if not( v1 == 0 and v2 == 0):
                            try:
                                couleur = self.canvas.itemcget(self.plateau[i+v1][j+v2],"fill")
                                if couleur == "black":
                                    res += 1
                            except:
                                pass
                ligne.append(res)
            v.append(ligne)
        return v

    def suiv(self):
        """
        Calcul du plateau suivant
        """
        self.iteration += 1

        voisins = self.voisinage()
        for i in range(0,self.taille):
            for j in range(0,self.taille):
                # Si elle a trois voisines, une cellule nait, si elle a deux ou trois voisines, elle survit, sinon elle meurt
                couleur = self.canvas.itemcget(self.plateau[i][j],"fill")
                if voisins[i][j] == 3 or (couleur == "black" and voisins[i][j] == 2):
                    self.canvas.itemconfig(self.plateau[i][j],fill="black")
                else:
                    self.canvas.itemconfig(self.plateau[i][j],fill="white")

    def start(self,event=None):
        """
        Programme qui arrête ou commence
        """
        if self.pause == 1:
            self.pause = 0
            self.startb.config(text="Pause")
        else:
            self.pause = 1
            self.startb.config(text="Start")

    def click(self,event=None):
        """
        Changement de la couleur d'un pixel
        """
        i = event.x // self.zoom
        j = event.y // self.zoom
        couleur = self.canvas.itemcget(self.plateau[i][j],"fill")
        if couleur == "black":
            self.canvas.itemconfig(self.plateau[i][j],fill="white")
        else:
            self.canvas.itemconfig(self.plateau[i][j],fill="black")

    def milliSecondUpdate(self,event=None):
        """
        Évolution temporelle
        """
        if self.pause == 0:
            self.suiv()
        # After 100 milliseconds update
        self.frame.after(100, self.milliSecondUpdate, self)

if __name__=='__main__':

    # Gestion des paramètres
    parser = argparse.ArgumentParser()
    parser.add_argument("-t", help="taille du plateau")
    args = parser.parse_args()

    if args.t:
        taille = int(args.t)
    else:
        taille = 40

    root = tkinter.Tk()
    root.title('Jeu de la vie')
    partie = Jeu(root, taille)
    root.mainloop()
