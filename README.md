# Game of life

Just a python script to emulate the game of life

![Game of life](doc/game-of-life.gif)

The [Conway's game of life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life) is a cellular automaton. Each cell of the graph respects three rules:
- a living cell with less than two neighbours dies
- a living cell with more than three neighbours dies
- a dead cell with three neighbours rises from death

The GUI allows to load some configurations (glider, flower, etc.). Moreover, it is possible to draw by yourself the pattern you want to study.

The script uses tkinter and needs to be installed:
```
pip3 install tkinter
```
To run the script:
```
python3 game_of_life.py -t SIZE
```

The t option defines the size of the interface.

# Langton's ant

The [Langton's ant](https://en.wikipedia.org/wiki/Langton%27s_ant) is a cellular automaton. It represents an ant moving according to three rules:
- if the ant is on a white cell, she goes right
- if the ant is on a black cell, she goes left
- the ant changes the color of the cell she left

The Langton's ant is a model based on very simple rules, but leading to very complex behaviour. After a first symmetric pattern, the ant enters in a pseudo-random phases. But after a long while (around 10 000 moves), a regular pattern emerges, called the highway.

The script uses tkinter and needs to be installed:
```
pip3 install tkinter
```
To run the script:
```
python3 langtons_ant.py -t SIZE
```

The t option defines the size of the interface.
