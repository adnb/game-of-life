#!/usr/bin/python3
# coding: utf8
#####################
# Fourmi de Langton #
#####################

import random
import argparse
import sys
import time
import tkinter

class Jeu:
    """
    Classe du jeu
    """
    def __init__(self, root, taille):
        # Taille du plateau
        self.taille = taille
        self.zoom = 10
        self.pause = 0
        self.iteration = 0
        self.plateau = list()
        # Position de la fourmi
        self.i = self.taille // 2
        self.j = self.taille // 2
        self.d = '^'

        self.frame = tkinter.Frame(root)
        self.frame.grid()
        self.frame.after(1, self.milliSecondUpdate, root)

        self.startb = tkinter.Button(self.frame, text="Start", fg="black", command=self.start)
        self.startb.grid(row=0,column=0)
        self.label = tkinter.Label(self.frame, text=str(self.iteration))
        self.label.grid(row=0,column=1)
        self.resetb = tkinter.Button(self.frame, text="Reset", fg="black", command=self.reset)
        self.resetb.grid(row=0,column=2)

        self.canvas = tkinter.Canvas(self.frame, width=self.taille*self.zoom, height=self.taille*self.zoom)
        self.canvas.grid(row=1,column=0,columnspan=3)

        self.canvas.bind('<Button-1>', self.click)

        self.reset()

    def reset(self):
        """
        Remplissage aléatoire du plateau
        """
        self.iteration = 0
        self.canvas.delete(tkinter.ALL)
        self.plateau = list()
        for i in range(0,self.taille):
            ligne = list()
            for j in range(0,self.taille):
                color = "white"
                rect = self.canvas.create_rectangle(i*self.zoom, j*self.zoom, (i+1)*self.zoom, (j+1)*self.zoom, fill=color)
                ligne.append(rect)
            self.plateau.append(ligne)

    def suiv(self):
        """
        Calcul du plateau suivant
        """
        self.iteration += 1
        self.label.config(text=str(self.iteration))

        couleur = self.canvas.itemcget(self.plateau[self.i][self.j],"fill")

        if couleur == "black":
            self.canvas.itemconfig(self.plateau[self.i][self.j],fill="white")
        else:
            self.canvas.itemconfig(self.plateau[self.i][self.j],fill="black")

        if couleur == "white":
            if self.d == '^':
                self.i += 1
                self.d = '>'
            elif self.d == '>':
                self.j -= 1
                self.d = 'v'
            elif self.d == 'v':
                self.i -= 1
                self.d = '<'
            elif self.d == '<':
                self.j += 1
                self.d = '^'
        else:
            if self.d == '^':
                self.i -= 1
                self.d = '<'
            elif self.d == '<':
                self.j -= 1
                self.d = 'v'
            elif self.d == 'v':
                self.i += 1
                self.d = '>'
            elif self.d == '>':
                self.j += 1
                self.d = '^'

    def start(self,event=None):
        """
        Programme qui arrête ou commence
        """
        if self.pause == 1:
            self.pause = 0
            self.startb.config(text="Pause")
        else:
            self.pause = 1
            self.startb.config(text="Start")

    def click(self,event=None):
        """
        Changement de la couleur d'un pixel
        """
        i = event.x // self.zoom
        j = event.y // self.zoom
        couleur = self.canvas.itemcget(self.plateau[i][j],"fill")
        if couleur == "black":
            self.canvas.itemconfig(self.plateau[i][j],fill="white")
        else:
            self.canvas.itemconfig(self.plateau[i][j],fill="black")

    def milliSecondUpdate(self,event=None):
        """
        Évolution temporelle
        """
        if self.pause == 0:
            self.suiv()
        # After 1 second, update
        self.frame.after(100, self.milliSecondUpdate, self)

if __name__=='__main__':

    # Gestion des paramètres
    parser = argparse.ArgumentParser()
    parser.add_argument("-t", help="taille du plateau")
    args = parser.parse_args()

    if args.t:
        taille = int(args.t)
    else:
        taille = 10

    root = tkinter.Tk()
    root.title('Fourmi de Langton')
    partie = Jeu(root, taille)
    root.mainloop()
